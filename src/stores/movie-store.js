import { defineStore } from "pinia";
import { initList } from "../helpers/constanta";

export const useMovieStore = defineStore("movie", {
  state: () => ({
    list: initList,
    genres: ["Drama", "Action", "Animation", "Sci-fi", "Horror"],
  }),
  getters: {
    getMovieById: (state) => {
      return (id) => state.list.find((item) => item.id === id);
    },
  },
  actions: {
    addList(movie) {
      this.list.push(movie);
    },
    setItem(newData) {
      const idx = this.list.findIndex((item) => item.id === newData.id);
      this.list[idx] = newData;
    },
    removeItem(id) {
      this.list = this.list.filter((item) => item.id !== id);
    },
  },
});
