import { v4 as uuidv4 } from "uuid";

export function generateId() {
  return uuidv4();
}
export function generateList(list) {
  let newList = [];

  list.map((item) => {
    newList.push({
      id: uuidv4(),
      title: item.title,
      director: item.director,
      summary: item.summary,
      genres: item.genres,
    });
  });
  return newList;
}

export function getGenreChipColor(genre) {
  if (genre.toLowerCase() === "drama") {
    return "orange";
  } else if (genre.toLowerCase() === "action") {
    return "deep-orange";
  } else if (genre.toLowerCase() === "animation") {
    return "cyan";
  } else if (genre.toLowerCase() === "sci-fi") {
    return "indigo";
  } else if (genre.toLowerCase() === "horror") {
    return "blue-grey";
  }
  return "#000";
}

export function getGenreChipIcon(genre) {
  if (genre.toLowerCase() === "drama") {
    return "fa-solid fa-heart-crack";
  } else if (genre.toLowerCase() === "action") {
    return "fa-solid fa-bullseye";
  } else if (genre.toLowerCase() === "animation") {
    return "fa-solid fa-cat";
  } else if (genre.toLowerCase() === "sci-fi") {
    return "fa-solid fa-atom";
  } else if (genre.toLowerCase() === "horror") {
    return "fa-solid fa-ghost";
  }
  return "fa-solid fa-ban";
}
