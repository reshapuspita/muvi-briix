export const initList = [
  {
    id: "831f6fa9-3cc7-4d72-90d6-0e4c4e067f07",
    title: "Inception",
    director: "Christopher Nolan",
    summary:
      "A thief who enters the dreams of others to steal secrets from their subconscious.",
    genres: ["Action", "Sci-fi", "Drama"],
  },
  {
    id: "aa1f6b13-f1c1-4cf3-8c3e-f3e0ac110426",
    title: "The Shawshank Redemption",
    director: "Frank Darabont",
    summary:
      "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
    genres: ["Drama"],
  },
  {
    id: "0128dc29-ee6c-4c95-a6b4-8141c0e4e9a4",
    title: "Alien",
    director: "Ridley Scott",
    summary:
      "A commercial crew aboard the deep space towing vessel, Nostromo, is on its way home when they pick up an SOS warning from a distant moon.",
    genres: ["Horror", "Sci-fi"],
  },
  {
    id: "af4d11d0-fb3c-4c8b-9b82-88bfc57a32b3",
    title: "The Godfather",
    director: "Francis Ford Coppola",
    summary:
      "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
    genres: ["Drama"],
  },
  {
    id: "2b88b95d-3765-44e6-86b8-03632d43a8b7",
    title: "Interstellar",
    director: "Christopher Nolan",
    summary:
      "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
    genres: ["Sci-fi", "Drama"],
  },
  {
    id: "6e78518c-1690-43a1-8428-1cbdcf6f4d3a",
    title: "The Silence of the Lambs",
    director: "Jonathan Demme",
    summary:
      "A young F.B.I. cadet must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer, a madman who skins his victims.",
    genres: ["Drama", "Action"],
  },
  {
    id: "b6b6b1b2-bc10-4ef3-8f08-f5ad00ee1a3d",
    title: "The Matrix",
    director: "Lana Wachowski, Lilly Wachowski",
    summary:
      "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
    genres: ["Action", "Sci-fi"],
  },
  {
    id: "f1bb6077-5428-442b-9c68-9fc465d30ff9",
    title: "The Shining",
    director: "Stanley Kubrick",
    summary:
      "A family heads to an isolated hotel for the winter where a sinister presence influences the father into violence, while his psychic son sees horrific forebodings from both past and future.",
    genres: ["Horror"],
  },
  {
    id: "0f1cbbf4-940e-4b43-8a7b-f6574b37f0fc",
    title: "Avatar",
    director: "James Cameron",
    summary:
      "A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
    genres: ["Action", "Drama", "Sci-fi"],
  },
  {
    id: "4d6a1fc5-1000-4711-9f61-f72e7a6f51b5",
    title: "The Dark Knight",
    director: "Christopher Nolan",
    summary:
      "When the menace known as The Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham.",
    genres: ["Action", "Drama"],
  },
  {
    id: "f01c2b29-af59-44b1-9c57-cd8913704cc9",
    title: "Toy Story",
    director: "John Lasseter",
    summary:
      "A cowboy doll is profoundly threatened and jealous when a new spaceman figure supplants him as top toy in a boy's room.",
    genres: ["Animation"],
  },
  {
    id: "7e2a63c0-d0c7-4b48-b8c8-d6da4c9d4f16",
    title: "Psycho",
    director: "Alfred Hitchcock",
    summary:
      "A Phoenix secretary embezzles $40,000 from her employer's client, goes on the run, and checks into a remote motel run by a young man under the domination of his mother.",
    genres: ["Horror"],
  },
  {
    id: "1561c3cd-95c3-4d9d-bba7-3a196c7f4f95",
    title: "The Lion King",
    director: "Roger Allers, Rob Minkoff",
    summary:
      "Lion prince Simba and his father are targeted by his bitter uncle, who wants to ascend the throne himself.",
    genres: ["Animation", "Drama"],
  },
];
